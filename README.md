该目录存放训练Bert-ranker所需要的数据



#### MSMARCO

- query集合：queries.tar.gz
  
  `wget https://msmarco.blob.core.windows.net/msmarcoranking/queries.tar.gz`
- passage集合：collection.tar.gz
  
  `wget https://msmarco.blob.core.windows.net/msmarcoranking/collection.tar.gz`
  
- qrels
  
  `wget https://msmarco.blob.core.windows.net/msmarcoranking/qrels.dev.tsv`
  
  `wget https://msmarco.blob.core.windows.net/msmarcoranking/qrels.train.tsv`
  
- train_v4_100K.pairs
  
  `wget https://msmarco.blob.core.windows.net/msmarcoranking/qidpidtriples.train.full.2.tsv.gz`
  
- 索引：index-msmarco-passage-20201117-f87c94.tar.gz
  
  `wget https://git.uwaterloo.ca/jimmylin/anserini-indexes/raw/master/index-msmarco-passage-20201117-f87c94.tar.gz`





valid.run: 从qrels中抽取100个query并补充100个bm25结果

valid_v2.run: 从top1000.dev.tsv中抽取100个query组

valid_v3.run: 从qrels.dev中抽取100个query对